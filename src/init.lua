local ZwaveDriver = require "st.zwave.driver"
local capabilities = require "st.capabilities"
local defaults = require "st.zwave.defaults"
local log = require "log"

--- @type st.zwave.CommandClass
local cc = require "st.zwave.CommandClass"

--- @type st.utils
local utils = require "st.utils"

--- @type st.zwave.constants
local constants = require "st.zwave.constants"

--- @type st.zwave.CommandClass.ThermostatSetpoint
local ThermostatSetpoint = (require "st.zwave.CommandClass.ThermostatSetpoint")({
    version = 1
})

--- @type st.zwave.CommandClass.Configuration
local Configuration = (require "st.zwave.CommandClass.Configuration")({
    version = 1
})

--- @type st.zwave.CommandClass.WakeUp
local WakeUp = (require "st.zwave.CommandClass.WakeUp")({
    version = 2
})

--- @type st.zwave.CommandClass.Association
local Association = (require "st.zwave.CommandClass.Association")({
    version = 1
})

local K_NEED_INIT = "need-init"
local K_CACHED_SETPOINT = "cached_setpoint"

local ZWAVE_FINGERPRINTS = {{
    -- SRT321
    mfr = 0x0059,
    prod = 0x0001,
    model = 0x0003
}}

local function can_handle(opts, driver, device, ...)
    for _, fingerprint in ipairs(ZWAVE_FINGERPRINTS) do
        if device:id_match(fingerprint.mfr, fingerprint.prod, fingerprint.model) then
            return true
        end
    end
    return false
end

local function set_heating_setpoint(driver, device, command)
    log.info("Entering set_heating_setpoint")

    local scale = device:get_field(constants.TEMPERATURE_SCALE)
    local value = command.args.setpoint
    if (scale == ThermostatSetpoint.scale.FAHRENHEIT) then
        value = utils.c_to_f(value) -- the device has reported using F, so set using F
    end

    local set = ThermostatSetpoint:Set({
        setpoint_type = ThermostatSetpoint.setpoint_type.HEATING_1,
        scale = scale,
        value = value
    })

    local unit = 'C'
    if (scale == ThermostatSetpoint.scale.FAHRENHEIT) then
        unit = 'F'
    end

    device:emit_event(capabilities.thermostatHeatingSetpoint.heatingSetpoint({
        value = value,
        unit = unit
    }))

    device:set_field(K_CACHED_SETPOINT, set)
end

local function update_preferences(self, device, args)
    if device:get_field(K_NEED_INIT) then
        -- battery reports
        device:send(Association:Set{
            grouping_identifier = 3,
            node_ids = {self.environment_info.hub_zwave_id}
        })

        -- setpoint reports
        device:send(Association:Set{
            grouping_identifier = 4,
            node_ids = {self.environment_info.hub_zwave_id}
        })

        -- temperature reports
        device:send(Association:Set{
            grouping_identifier = 5,
            node_ids = {self.environment_info.hub_zwave_id}
        })

        -- send temperature reports every 0.5 step
        device:send(Configuration:Set{
            parameter_number = 3,
            default = false,
            size = 1,
            configuration_value = 5
        })

        device:set_field(K_NEED_INIT, false)
    end

    if device.preferences.reportingInterval ~= nil and args.old_st_store.preferences.reportingInterval ~=
        device.preferences.reportingInterval then
        device:send(WakeUp:IntervalSet({
            node_id = self.environment_info.hub_zwave_id,
            seconds = device.preferences.reportingInterval * 60
        }))
    end

    local cached_setpoint_command = device:get_field(K_CACHED_SETPOINT)
    if cached_setpoint_command ~= nil then
        device:send(cached_setpoint_command)
        device:set_field(K_CACHED_SETPOINT, nil)
    end
end

local function init(self, device)
    device:set_field(K_NEED_INIT, true)
    device:set_update_preferences_fn(update_preferences)
end

local driver_template = {
    NAME = "SRT321",
    supported_capabilities = {capabilities.thermostatHeatingSetpoint, capabilities.temperatureMeasurement,
                              capabilities.battery, capabilities.refresh},
    capability_handlers = {
        [capabilities.thermostatHeatingSetpoint.ID] = {
            [capabilities.thermostatHeatingSetpoint.commands.setHeatingSetpoint.NAME] = set_heating_setpoint
        }
    },
    zwave_handlers = {},
    lifecycle_handlers = {
        init = init
    },
    can_handle = can_handle
}

defaults.register_for_default_handlers(driver_template, driver_template.supported_capabilities)
local driver = ZwaveDriver("secure-srt", driver_template)
driver:run()
