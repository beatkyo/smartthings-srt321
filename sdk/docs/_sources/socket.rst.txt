Socket
------

The socket library implements (most of) the `LuaSocket
<http://w3.impa.br/~diego/software/luasocket/reference.html>`_ API.

.. lua:automodule:: socket
