SmartThings Edge Device Drivers Reference Documentation
=======================================================

Below you can find reference documentation for SmartThings Edge Device Driver libraries, modules, data classes, and more.

Versions
--------

Version 0 [Public Beta (hubcore 0.40.X)] - **2021-11-15**
++++++++++++++++++++++++++++++++++++++++

Initial public release

.. toctree::
   :maxdepth: 2
   :caption: References:

   reference/index.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
